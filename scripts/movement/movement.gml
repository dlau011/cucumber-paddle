function npcMovement(){
	// this script gets called in step event when state == States.moveToDestination'
	// it makes the npc move_towards_point
		// once they reach that point they can attack or find another tile to move to
	if (point_distance(curTile.x, curTile.y, x, y) == 0) {
		// alarm[1] readys attack but we have to be in valid spot
		if readyAttack {
			speed = 0
			readyAttack = false
			state = States.attack
			var activationDelay = attackScript() // returns how long it takes for the attack to activate
			// TODO maybe handle next attack, movedelay, and alarm here
			alarm[1] = activationDelay + attackSpeed
		} else {
			// move left or right
			if (curTile.gridY == 0) {
				// boundary handling
				if (curTile.gridX == objBattleController.width - 1) {
					moveDirection = Direction.west
				} else if (curTile.gridX == 1) {
					moveDirection = Direction.east	
				}
				// move left until we hit boundary
				if (moveDirection == Direction.west) {
					image_xscale = -1
					curTile = ds_grid_get(objBattleController.battleGrid, curTile.gridX - 1, curTile.gridY)
				} else {
					image_xscale = 1
					curTile = ds_grid_get(objBattleController.battleGrid, curTile.gridX + 1, curTile.gridY)
				}
			// move up and down
			} else {
				// boundary handling
				if (curTile.gridY == objBattleController.height - 1) {
					moveDirection = Direction.north
				} else if (curTile.gridY == 1) {
					moveDirection = Direction.south	
				}
				// if we chose to move up and are able to move up OR we cannot move down
				if (moveDirection == Direction.north) {
					curTile = ds_grid_get(objBattleController.battleGrid, curTile.gridX, curTile.gridY - 1)
				} else {
					curTile = ds_grid_get(objBattleController.battleGrid, curTile.gridX, curTile.gridY + 1)
				}
			}
			move_towards_point(curTile.x, curTile.y, moveSpeed)
		}
	} else if point_distance(curTile.x, curTile.y, x, y) < moveSpeed {
		x = curTile.x
		y = curTile.y
	}
}
