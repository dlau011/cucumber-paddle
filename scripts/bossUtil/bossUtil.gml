function incrementState() {
	// do all the things in the active statelist
	if (currentStateList != undefined && stateIndex < ds_list_size(currentStateList) - 1) {
		stateIndex += 1	
		state = currentStateList[|stateIndex]
	// done with the list
	} else {
		currentStateList = undefined
		stateIndex = 0
		state = States.chase
	}
}

function startStateList(stateList) {
	currentStateList = stateList
	state = currentStateList[|0]
}

function pickAttack() {
	var choices = undefined
	switch(object_index){
		case npcBossGhost:
			if (stage == 0) {
				//choices = [bossGhostErupt, bossGhostCharge, bossGhostFurniture]
				choices = [bossGhostFurniture]
			}
	}
	nextAttack = choices[irandom(array_length(choices)-1)]
}

function moveToDestination() {
	move_towards_point(destinationX, destinationY, moveSpeed)
	//  keep checking if we need to stop
	var distanceToDestination = point_distance(x, y, destinationX, destinationY)
	if (distanceToDestination <= moveSpeed) {
		// this prevents overshooting the tile
		x = destinationX
		y = destinationY
		speed = 0
		destinationX = undefined
		destinationY = undefined
		incrementState() // start rotating in place
	}
}
function randInt(a, b) {
	var i = irandom_range(a, b)	
	show_debug_message("random int between " + string(a) + "," + string(b) + ": " + string(i))
	return i
}

