// state methods for erupt
function bossGhostErupt() {
	startStateList(eruptionAttackStates)
}

// stand still and wait 
function prepareEruption() {
	if (alarm[1] <= 0) {
		var startEruptionDelay = 100 // how long to stand in place
		speed = 0
		alarm[1] = 	startEruptionDelay 
	}
}

// randomly pick tiles in the battlegrid to apply bossAttackStatus to
function erupt() {
	if (alarm[1] <= 0) {
		var hitChance = .33 // chance a tile will be erupted
		var activationDelay = 120 // frames before deactivation
		
		// randomly pick tiles
		with(objBattleController) {
			for (var h=1; h<height-1; h++) {
				for (var w=1; w<width-1; w++) {
					// inner tiles only
					var baseTile = ds_grid_get(battleGrid, w, h)
					var hit = random_range(0,1)
					if (hit <= hitChance) {
						createTileWithId(baseTile, objGhostEruptionTile, activationDelay, other.id)
					}
				}
			}
		}
		if (alarm[1] <= 0) {
			alarm[1] = 60 // nextState
			alarm[2] = attackSpeed // nextAttack
		}
	}
}
