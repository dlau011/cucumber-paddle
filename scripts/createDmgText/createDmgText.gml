// display any healing/damage/etc
function createDmgText(textToWrite, targetId, textColor){
	with(instance_create_layer(0, 0, "Status", objDmgText)) {
		text = string(textToWrite)
		target = targetId
		color = textColor
	}
}