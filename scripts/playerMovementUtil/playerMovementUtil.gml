// run step logic for player movement
function playerKnockbackHandler() {
    if (point_distance(x, y, destinationX, destinationY) <= 8) {
        x = destinationX
        y = destinationY
        destinationX = undefined
        destinationY = undefined
        state = States.battle
        speed = 0
    } else {
        move_towards_point(destinationX, destinationY, 8)
    }
}

function calculateKnockback() {
	destinationX = x - (other.x - x)
	destinationY = y - (other.y - y)
	while (place_meeting(destinationX, y, objCollision)) {
		destinationX -= sign(destinationX - x)
	}
	while (place_meeting(x, destinationY, objCollision)) {
		destinationY -= sign(destinationY - y)
	}
}

function playerMovementHandler() {
	//Get Input
	input_up =		keyboard_check(ord("W"))
	input_down =	keyboard_check(ord("S"))
	input_left =	keyboard_check(ord("A"))
	input_right =	keyboard_check(ord("D"))


	//Get intended movement
	moveY = (input_down - input_up)*rpgMoveSpeed
	moveX = (input_right - input_left)*rpgMoveSpeed

	//Collision Check Horizontal
	if(place_meeting(x+moveX, y, objCollision))
	{
		repeat(abs(moveX))
		{
			if(!place_meeting(x+sign(moveX), y, objCollision)){x += sign(moveX)}
			else {break}
		}
		moveX = 0
	}

	//Collision Check Vertical
	if(place_meeting(x, y+moveY, objCollision))
	{
		repeat(abs(moveY))
		{
			if(!place_meeting(x, y+sign(moveY), objCollision)){y += sign(moveY)}
			else {break}
		}
		moveY = 0
	}

	//Apply movement
	x += moveX
	y += moveY
	#region Sprite change per direction

	//UP
	if (moveY < 0){sprite_index = sprRPGPlayerUp}
	//DOWN
	if (moveY > 0){sprite_index = sprRPGPlayerDown}
	//LEFT
	if (moveX < 0){sprite_index = sprRPGPlayerLeft}
	//RIGHT
	if (moveX > 0){sprite_index = sprRPGPlayerRight}

	#endregion

	#region Animation
	//When moving the player sprite will animate walking
	if (moveX == 0 and moveY == 0){key_pressed = false} else {key_pressed = true}
	if (key_pressed && state != States.dialogue) {
		image_speed = 0.3
	} else {
		image_speed = 0
	}

	#endregion
}