function bossGhostFurniture(){
	startStateList(furnitureKnockbackStates)
}

function stopAndGlow() {
	if (alarm[1] <= 0) {
		speed = 0
		sprite_index = sprBossGhostGlow
		var furniture = instance_find(objBossGhostFurniture, irandom(instance_number(objBossGhostFurniture) - 1));
		furniture.sprite_index = sprBossGhostChairGlow
		destinationX = furniture.x
		destinationY = furniture.y
		moveSpeed = 20
		alarm[1] = 120 // furniture glow, then charge
	}
}

function endFurnitureKnockback() {
	if (alarm[1] <= 0) {
		moveSpeed = defaultMoveSpeed
		sprite_index = sprBossGhost
		with (objBossGhostFurniture) {
			if (sprite_index = sprBossGhostChairGlow) {
				sprite_index = sprBossGhostChair
			}
		}
		alarm[1] = 60 // wait to chase player
		alarm[2] = 120
	}
}

function createFurniture() {
    // for every 3 rows in the grid, pick a spot to spawn furniture
    for (var h=2; h < ds_grid_height(battleGrid)-1; h=h+2) {
        var w = 1 + max(0, irandom(ds_grid_width(battleGrid)-3))
        var curTile = ds_grid_get(battleGrid, w, h)
        instance_create_layer(curTile.x, curTile.y, "BattleGrid", objBossGhostChair)
    }
}