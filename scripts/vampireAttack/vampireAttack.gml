// straight line across

function vampireAttack(){
	var attackToCall = attackScripts[| irandom_range(0, ds_list_size(attackScripts) - 1)]
	return attackToCall()
}

// 2x3 rectangle of blood
function vampireAttackBloodSuck() {
	var activationDelay = 120 // how long to warn
	var moveDelay = activationDelay + 20 // when to move after attack is done
	if (curTile.gridY == 0) { // moving left and right
		// attack (x-1, y+1), (x, y+1), (x+1, y+1) and (x-1, y+2), (x, y+2), (x+1, y+2)
		for (var i = max(1, curTile.gridX - 1); i <= min(objBattleController.width-1, curTile.gridX + 1); i++) {
			var baseTile = ds_grid_get(objBattleController.battleGrid, i, curTile.gridY + 1)
			createTile(baseTile, objBloodTile, activationDelay)
			baseTile = ds_grid_get(objBattleController.battleGrid, i, curTile.gridY + 2)
			createTile(baseTile, objBloodTile, activationDelay)
		}
	} else { // moving up and down
		// attack (x+1, y-1), (x+1, y), (x+1, y+1) and (x+2, y-1), (x+2, y), (x+2, y+1)
		for (var i = max(1, curTile.gridY - 1); i <= min(objBattleController.height - 1, curTile.gridY + 1); i++) {
			var baseTile = ds_grid_get(objBattleController.battleGrid, curTile.gridX + 1, i)	
			createTile(baseTile, objBloodTile, activationDelay)
			baseTile = ds_grid_get(objBattleController.battleGrid, curTile.gridX + 2, i)
			createTile(baseTile, objBloodTile, activationDelay)
		}
	}
	state = States.idle
	alarm[0] = moveDelay
	return activationDelay
}

// line of blood that stays for a while
function vampireAttackBloodDrain(){
	var activationDelay = 180 // when to destroy tile
	var moveDelay = activationDelay + 60 // when to move after tiles are created
	randomize()
	var tileEffected = false // ensures at least one tile is picked
	// if moving left and right
	if (curTile.gridY == 0) {
		// attack the entire column
		for (var i = 1; i < objBattleController.height; i++) {
			if (irandom_range(0, 1) || (!tileEffected && i == objBattleController.height - 1)) {
				tileEffected = true
				var baseTile = ds_grid_get(objBattleController.battleGrid, curTile.gridX, curTile.gridY + i)
				createTile(baseTile, objBloodTile, activationDelay)
			}
		}
	// moving up and down
	} else {
		for (var i = 1; i < objBattleController.width; i++) {
			if (irandom_range(0, 1) || (!tileEffected && i == objBattleController.width - 1)) {
				tileEffected = true
				var baseTile = ds_grid_get(objBattleController.battleGrid, curTile.gridX + i, curTile.gridY)
				createTile(baseTile, objBloodTile, activationDelay)
			}
		}
	}
	state = States.idle
	alarm[0] = moveDelay // start moving after the tiles are inactive
	return activationDelay
}
