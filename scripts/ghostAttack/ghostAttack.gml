
function ghostAttack(){
	var attackToCall = attackScripts[| irandom_range(0, ds_list_size(attackScripts) - 1)]
	return attackToCall()
}

// bullet in straight line across that damages boss 
function ghostAttackProjectile() {
	// have him sit still and have the projectile wait to be created
	var moveDelay = 120 // when to move after shooting
	// shooting from top row
	if (curTile.gridY == 0) {
		with(instance_create_layer(x, y, "Projectiles", objGhostProjectile)) {
			direction = 270
		}
	// shooting from left column
	} else {
		with(instance_create_layer(x, y, "Projectiles", objGhostProjectile)) {
			direction = 0
		}
	}
	state = States.idle
	alarm[0] = moveDelay
	return moveDelay
}

// straight line of tiles across that slows the boss and heals the player
function ghostAttackEctoplasm() {
	var activationDelay = 120 // how long to warn
	var moveDelay = 120 // when to move after creating the tile
	// if moving left and right
	if (curTile.gridY == 0) {
		// attack the entire column
		for (var i = 1; i < objBattleController.height; i++) {
			var baseTile = ds_grid_get(objBattleController.battleGrid, curTile.gridX, curTile.gridY + i)
			createTile(baseTile, objEctoplasmTile, activationDelay)
		}
	// moving up and down
	} else {
		for (var i = 1; i < objBattleController.width; i++) {
			var baseTile = ds_grid_get(objBattleController.battleGrid, curTile.gridX + i, curTile.gridY)
			createTile(baseTile, objEctoplasmTile, activationDelay)
		}
	}
	state = States.idle
	alarm[0] = moveDelay // start moving after the tiles are inactive
	return activationDelay
}
