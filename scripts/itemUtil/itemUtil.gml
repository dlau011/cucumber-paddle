// open item dialogue on pickup
function pickUpItem(objItem) {
	with(instance_create_layer(x, y, "Text", objItemTextBox)) {
		item = objItem
	}
}

function getItemFromNpc(itemName) {
	item = asset_get_index(itemName)
	if item > -1 {
		object_set_visible(item, false)
		newItem = instance_create_layer(0, 0, "player", item)
		object_set_visible(item, true)
		pickUpItem(newItem)
	} else {
		show_debug_message("itemUtil.getItemFromNpc could not find asset with itemName: " + itemName)	
	}
}

function getSaveDataKey() {
	return room_get_name(room) + object_get_name(object_index) + string(x) + string(y)
}

function playerHasItem(itemName) {
	pos = ds_list_find_index(getPlayerInventory(), itemName)
	if (pos >= 0) {
		// TODO split out so you dont necessarily lose the item when checked
		ds_list_delete(getPlayerInventory(), pos)
		return true
	}
	return false
}
