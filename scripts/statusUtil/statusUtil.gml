// when player/boss triggers collision with a tile, we check each status in the objTileBase.statuses dict
// and trigger their respective collision scripts
function bloodCollision(collider) {
	if (!collider.invulnerable) {
		// affects both player and boss
		with (instance_create_layer(x,y,"Status", objDoTHandler)) {
			damage = other.damage
			target = collider
			alarm[0] = 1 // triggers dots 
		}
		collider.invulnerable = true
	}
}

function ectoBloodCollision(collider) {
	if (collider.object_index == objPlayerController) {
		// healing
		if (!collider.recentlyHealed) {
			collider.curHp = min(collider.curHp + heal, collider.maxHp)
			collider.recentlyHealed = true
			createDmgText(string(heal), collider, c_green)
		}
		// damage reduction
		if (!collider.recentlyDmgReduced) {
			with (instance_create_layer(x,y, "Status", objDmgReductionHandler)) {
				target = collider
				dmgReduction = other.dmgReduction
			}
			collider.dmgTaken -= dmgReduction
			collider.recentlyDmgReduced = true
		}
	} else if (object_is_ancestor(collider.object_index, objNpcBoss)) {
		if (!collider.invulnerable) {
			var netDmg = round(damage*collider.dmgTaken)
			collider.curHp = max(0, collider.curHp - netDmg)
			createDmgText(string(netDmg), collider, c_red)
			if (collider.canBeStunned) {
				collider.canBeStunned = false
				with (instance_create_layer(x, y, "Status", objStunHandler)) {
					originalMovespeed = collider.moveSpeed
					target = collider
					alarm[0] = other.stunTime // currently not dynamic
				}
				collider.speed = 0
				collider.currentStateList = undefined
				collider.state = States.idle
			}
			collider.invulnerable = true
		}
	}
}
function ectoCollision(collider) {
	// heals player
	if (collider.object_index == objPlayerController) {
		if (!collider.recentlyHealed) {
			collider.curHp = min(collider.curHp + heal, collider.maxHp)
			createDmgText(string(heal), collider, c_green)
			collider.recentlyHealed = true
		}
	} else if (object_is_ancestor(collider.object_index, objNpcBoss)) {
		if (!collider.invulnerable) {
			var netDmg = round(damage*collider.dmgTaken)
			collider.curHp = max(0, collider.curHp - netDmg)
			createDmgText(string(netDmg), id, c_red)
			if (collider.canBeSlowed) {
				collider.canBeSlowed = false
				with (instance_create_layer(x, y, "Status", objSlowHandler)) {
					originalMovespeed = collider.moveSpeed
					target = collider
					alarm[0] = other.slowTime
				}
				collider.moveSpeed *= slow
			}
			collider.invulnerable = true
		}
	}
}

function bossAttackCollision(collider) {
	if (collider.object_index == objPlayerController) {
		if (!collider.invulnerable) { 
			var dmg = round(collider.dmgTaken * damage)
			collider.curHp -= dmg
			createDmgText(string(dmg), collider, c_white)
			collider.invulnerable = true
		}
	
	}
}

function getStatus(curTile, status) {
	return curTile.statuses[? status]	
}

function createStatus(status) {
	var newStatus = noone
	var duration = 300 // TODO determine how long status lasts
	with (instance_create_layer(x, y, "Status", status)) {//(x - curTile.sprite_width*.25, y - curTile.sprite_height*.25, "Status", status)) {
		curTile = other.curTile
		alarm[0] = duration
		newStatus = id
	}
	return newStatus
}


function createBorderStatus(status) {
	var newStatus = noone
	var duration = 300 // TODO determine how long status lasts
	with (instance_create_layer(x, y, "Status", status)) {//(x - curTile.sprite_width*.25, y - curTile.sprite_height*.25, "Status", status)) {
		curTile = other.curTile
		alarm[0] = duration
		newStatus = id
		image_xscale = 2
		image_yscale = 2
	}
	return newStatus
	
}


