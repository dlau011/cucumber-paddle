// label can be passed in 3 ways
// 1. manually when you trigger a convo e.g. when you pick up an item
// 2. automatically when you talk to an npc and trigger their "nextDialogue" label
// 3. when you have a dialogue that continues and finds the "next" attribute from the json text file
function getConversation(npc, label){
	npc.state = States.dialogue
	objPlayerController.state = States.dialogue
	// grab the npc.nextDialogue when the PC talks to the NPC to start the convo
	var conv = undefined
	switch(npc.object_index){
		case npcDog:
			conv = global.dialogueDog
			break
		case npcBossGhost:
			conv = global.dialogueGhostBoss
			if playerHasItem("objTestItem") { 
				label = "intro.withItem"
			}
			break
		case npcCultist:
			conv = global.dialogueCultist
			break
	}
	if (conv == undefined) {
		show_debug_message("could not find conversation " + label + " for npc " + npc.name)
	} else {
		createConversation(ds_map_find_value(conv, label), npc)
	}
}

function createConversation(conv, npc) {
	with(instance_create_layer(0,0, "Text", objTextBox)) {
		originalNpc = npc
		speaker = originalNpc
		// have objTextBox display each [id, text] in text
		texts = conv[?"text"]
		// when it's done, display each choice in choices assuming objPlayerController is speaker
		choices = conv[?"choices"]
		// on selection, call getConversation(npc, branches[choice])
		branches = conv[?"branches"]
		// npc.nextDialogue
		next = conv[?"next"]
		// will trigger battleUtil to start a battle 
		battle = conv[?"battle"]
		// gives player item
		item = conv[?"item"]
	}
}

/// wrap the dialogue before it breaks in the middle of a word
function wrapDialogue(str, maxWidth){
	if (str != undefined) {
		var strLen = string_length(str)
		var lastSpace = 1
		var count = 1
		var tempStr
		repeat(strLen) {
			// grab substring and see if we will exceed max length
			tempStr = string_copy(str, 1, count)
			if (string_char_at(str, count) == " ") {
				lastSpace = count	
			}
			if (string_width(tempStr) > maxWidth) {
				str = string_delete(str, lastSpace, 1)
				str = string_insert("\n", str, lastSpace)	
				count += 1
			}
			count++	
		}
		return str
	}
}