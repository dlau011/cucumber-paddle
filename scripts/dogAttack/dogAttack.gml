// heal that goes across the entire column/row
// alarm[1] for attacking
function dogAttack(){
	var activationDelay = 180 // how long to warn
	var moveDelay = activationDelay + 20// when to move after statuses are done
	// if moving left and right
	if (curTile.gridY == 0) {
		// attack the entire column
		for (var i = 1; i < objBattleController.height; i++) {
			var baseTile = ds_grid_get(objBattleController.battleGrid, curTile.gridX, curTile.gridY + i)
			createTile(baseTile, objHealTile, activationDelay,)
		}
	// moving up and down
	} else {
		// attack the entire row
		for (var i = 1; i < objBattleController.width; i++) {
			var baseTile = ds_grid_get(objBattleController.battleGrid, curTile.gridX + i, curTile.gridY)	
			createTile(baseTile, objHealTile, activationDelay)
		}
	}
	state = States.idle
	alarm[0] = moveDelay // start moving after the tiles are inactive
	alarm[1] = attackSpeed
}
