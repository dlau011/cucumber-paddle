// activated tiles use stats from their activators to make/combine objStatus
// statuses determine what happens when the player/boss interacts with an objBaseTile

function removeAllStatuses(curTile) {
	for (var i = 0; i < TileStatus.numTileStatuses; i++) {
		ds_map_delete(curTile.statuses, i)
	}
}

function activateBloodTile(curTile, activator) {
	var ectoStatus = getStatus(curTile, TileStatus.ectoplasm)
	// blood + ecto = ectoBlood
	if (ectoStatus != undefined) {
		var newStatus = createStatus(objStatusEctoBlood)
		newStatus.heal = ectoStatus.heal*2
		newStatus.damage = ectoStatus.damage + activator.damage // combine blood and ecto damage
		curTile.statuses[? TileStatus.ectoBlood] = newStatus
		instance_destroy(ectoStatus)
	// blood
	} else {
		var newStatus = createStatus(objStatusBlood)
		newStatus.damage = activator.damage// blood's DoT = vampire's damage
		curTile.statuses[? TileStatus.blood] = newStatus
	}
}

function activateEctoplasmTile(curTile, activator) {
	// ecto + blood = ectoBlood
	var bloodStatus = getStatus(curTile, TileStatus.blood)
	if (bloodStatus != undefined) {
		var newStatus = createStatus(objStatusEctoBlood) 
		newStatus.heal = activator.heal*2
		newStatus.damage = bloodStatus.damage + activator.damage // combine blood and ecto damage
		curTile.statuses[? TileStatus.ectoBlood] = newStatus
		instance_destroy(bloodStatus)
	} else {
		var newStatus = createStatus(objStatusEctoplasm)
		newStatus.damage = activator.damage
		newStatus.heal = activator.heal
		curTile.statuses[? TileStatus.ectoplasm] = newStatus
	}
}

function activateEruptionTile(curTile, activator) {
	removeAllStatuses(curTile)
	var newStatus = createBorderStatus(objStatusBossAttack)
	curTile.statuses[? TileStatus.bossAttack] = newStatus
	newStatus.damage = activator.eruptionDamage	
}

function activateHealTile(curTile, activator) {
	return
}


// use if calling createTile outside of creator context e.g. ghost erupt
function createTileWithId(baseTile, tileType, activationDelay, creatorId) {
	with (creatorId) {
		createTile(baseTile, tileType, activationDelay)	
	}
}

function createTile(baseTile, tileType, activationDelay) {
	with (instance_create_layer(baseTile.x, baseTile.y, "BattleGrid", tileType)) {
		curTile = baseTile
		activator = other
		alarm[0] = activationDelay
	}
}