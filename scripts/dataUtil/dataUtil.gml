// helper methods to retrieve save data

// returns inventory. creates new inventory if not found
function getPlayerInventory() {
	// get str data
	var inventory = objRPGController.saveData[? "playerInventory"]
	if inventory == undefined { 
		inventory = ds_list_create()
		ds_map_add_list(objRPGController.saveData, "playerInventory", inventory)
	}
	return inventory
}

function addPlayerInventory(item) {
	ds_list_add(getPlayerInventory(), object_get_name(item.object_index))	
}