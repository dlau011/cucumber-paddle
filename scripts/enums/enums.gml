enum States {
	// general States
	dialogue,
	rpg,
	battle,
	idle,
	moveToDestination,
	attack,
	knockback,
	// general boss States
	chase,
	// GHOST BOSS
		//charge attack
		rotateInPlace,
		chargeEdge,
		// eruption attack
		prepareEruption,
		erupt,
		// furniture
		stopAndGlow,
		endFurnitureKnockback
}

enum Direction {
	north, east, south, west
}
