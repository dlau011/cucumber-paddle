function importJson(fname){
	if (file_exists(fname)) {
		var file, json
		file = file_text_open_read(fname)
		json = ""
		while (!file_text_eof(file)) {
			json += file_text_read_string(file)
			file_text_readln(file)
		}
		file_text_close(file)
		return json_decode(json)
	}
	return ""
}
