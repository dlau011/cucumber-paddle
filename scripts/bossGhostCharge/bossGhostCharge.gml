// defines functions for each state after starting the statelist
function bossGhostCharge() {
	curPath = irandom(array_length(objBattleController.ghostChargePaths)-1)
	destinationX = path_get_point_x(objBattleController.ghostChargePaths[curPath], 0)
    destinationY = path_get_point_y(objBattleController.ghostChargePaths[curPath], 0)
	moveSpeed = 4
	path_position = 0 
	startStateList(chargeAttackStates)
}


function rotateInPlace() {
	// rotate until alarm changes state
	if (curPath < array_length(objBattleController.ghostChargePaths) / 2) {
        image_angle += 10
    } else {
        image_angle -=10
    }
	if (alarm[1] <= 0) {
		alarm[1] = 36 
		image_angle = 0
	}
}

function chargeEdge() {
	if (alarm[1] <= -1) {
		curPath = undefined
		moveSpeed = defaultMoveSpeed
		var delay = 60
		alarm[1] = delay // wait a bit between ending charge and chasing player
		alarm[2] = delay + attackSpeed // when to attack
	} else if (path_index == -1) {
		path_start(objBattleController.ghostChargePaths[curPath], 8, path_action_stop, true) // start path on state start
	}
}

// called as postSetupScript
function createChargePaths() {
    var chargeSpd = 200
    var h = ds_grid_height(battleGrid)
    var w = ds_grid_width(battleGrid)
    var topRightTile = ds_grid_get(battleGrid, w-1, 1)
    var topLeftTile = ds_grid_get(battleGrid, 1, 1)
    var bottomLeftTile = ds_grid_get(battleGrid, 1, h-1)
    var bottomRightTile = ds_grid_get(battleGrid, w-1, h-1)
    // top right cc
    var topRightCC = path_add()
    path_add_point(topRightCC, topRightTile.x, topRightTile.y, chargeSpd)
    path_add_point(topRightCC, topLeftTile.x, topLeftTile.y, chargeSpd)
    path_add_point(topRightCC, bottomLeftTile.x, bottomLeftTile.y, chargeSpd)
    path_add_point(topRightCC, bottomRightTile.x, bottomRightTile.y, chargeSpd)
    path_add_point(topRightCC, topRightTile.x, topRightTile.y, chargeSpd)
    // top left cc
    var topLeftCC = path_add()
    path_add_point(topLeftCC, topLeftTile.x, topLeftTile.y, chargeSpd)
    path_add_point(topLeftCC, bottomLeftTile.x, bottomLeftTile.y, chargeSpd)
    path_add_point(topLeftCC, bottomRightTile.x, bottomRightTile.y, chargeSpd)
    path_add_point(topLeftCC, topRightTile.x, topRightTile.y, chargeSpd)
    path_add_point(topLeftCC, topLeftTile.x, topLeftTile.y, chargeSpd)
    // bottom left cc
    var bottomLeftCC = path_add()
    path_add_point(bottomLeftCC, bottomLeftTile.x, bottomLeftTile.y, chargeSpd)
    path_add_point(bottomLeftCC, bottomRightTile.x, bottomRightTile.y, chargeSpd)
    path_add_point(bottomLeftCC, topRightTile.x, topRightTile.y, chargeSpd)
    path_add_point(bottomLeftCC, topLeftTile.x, topLeftTile.y, chargeSpd)
    path_add_point(bottomLeftCC, bottomLeftTile.x, bottomLeftTile.y, chargeSpd)
    // bottom right cc
    var bottomRightCC = path_add()
    path_add_point(bottomRightCC, bottomRightTile.x, bottomRightTile.y, chargeSpd)
    path_add_point(bottomRightCC, topRightTile.x, topRightTile.y, chargeSpd)
    path_add_point(bottomRightCC, topLeftTile.x, topLeftTile.y, chargeSpd)
    path_add_point(bottomRightCC, bottomLeftTile.x, bottomLeftTile.y, chargeSpd)
    path_add_point(bottomRightCC, bottomRightTile.x, bottomRightTile.y, chargeSpd)
    // top right cw
    var topRightCW = path_add()
    path_add_point(topRightCW, topRightTile.x, topRightTile.y, chargeSpd)
    path_add_point(topRightCW, bottomRightTile.x, bottomRightTile.y, chargeSpd)
    path_add_point(topRightCW, bottomLeftTile.x, bottomLeftTile.y, chargeSpd)
    path_add_point(topRightCW, topLeftTile.x, topLeftTile.y, chargeSpd)
    path_add_point(topRightCW, topRightTile.x, topRightTile.y, chargeSpd)
    // top left cw
    var topLeftCW = path_add()
    path_add_point(topLeftCW, topLeftTile.x, topLeftTile.y, chargeSpd)
    path_add_point(topLeftCW, topRightTile.x, topRightTile.y, chargeSpd)
    path_add_point(topLeftCW, bottomRightTile.x, bottomRightTile.y, chargeSpd)
    path_add_point(topLeftCW, bottomLeftTile.x, bottomLeftTile.y, chargeSpd)
    path_add_point(topLeftCW, topLeftTile.x, topLeftTile.y, chargeSpd)
    // bottom left cw
    var bottomLeftCW = path_add()
    path_add_point(bottomLeftCW, bottomLeftTile.x, bottomLeftTile.y, chargeSpd)
    path_add_point(bottomLeftCW, topLeftTile.x, topLeftTile.y, chargeSpd)
    path_add_point(bottomLeftCW, topRightTile.x, topRightTile.y, chargeSpd)
    path_add_point(bottomLeftCW, bottomRightTile.x, bottomRightTile.y, chargeSpd)
    path_add_point(bottomLeftCW, bottomLeftTile.x, bottomLeftTile.y, chargeSpd)
    // bottom right cw
    var bottomRightCW = path_add()
    path_add_point(bottomRightCW, bottomRightTile.x, bottomRightTile.y, chargeSpd)
    path_add_point(bottomRightCW, bottomLeftTile.x, bottomLeftTile.y, chargeSpd)
    path_add_point(bottomRightCW, topLeftTile.x, topLeftTile.y, chargeSpd)
    path_add_point(bottomRightCW, topRightTile.x, topRightTile.y, chargeSpd)
    path_add_point(bottomRightCW, bottomRightTile.x, bottomRightTile.y, chargeSpd)
    
    objBattleController.ghostChargePaths = [topRightCC, topLeftCC, bottomLeftCC, bottomRightCC,
        topRightCW, topLeftCW, bottomLeftCW, bottomRightCW]
}