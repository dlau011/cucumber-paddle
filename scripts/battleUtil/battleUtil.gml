function goToBattle(battleName){
	show_debug_message("starting battle: " + battleName)
	getBattle(battleName)
	instance_deactivate_object(objPlayerController)
	with(instance_create_layer(0, 0, "Fade", objRoomFade)) {
		targetRoom = battleRoom	
	}
}

function getBattle(battleName) {
	switch(battleName){
		case "ghost.battle":
			with(objBattleController) {
				startState = States.chase
				boss = npcBossGhost
				// if we stop persisting npcs we can set the health here
				boss.curHp = boss.maxHp
				boss.state = 0
				// at <= 5/6 health he will trigger dialogue "intro.battle2"
				boss.dialogueThreshold = 5/6
				leftNpc = npcGhost
				topNpc = objVampire
				postSetupScript = bossGhostSetup
			}
			break
		case "ghost.battle2":
			with(objBattleController) {
				startState = States.chase
				boss = npcBossGhost
				boss.state = 1
				boss.dialogueThreshold = 0
				leftNpc = npcGhost
				topNpc = objVampire
			}
			break
		default:
			show_debug_message("no battle found for battleName: " + battleName)
			break
	}
}

function resolveBattle(npc) {
	// reset the controller for the next battle
	with(objBattleController) {
		boss.state = States.rpg
		boss.sprite_index = boss.defaultSprite
		startState = undefined
		dialogueThreshold = 
		leftNpc.state = States.rpg
		leftNpc = undefined
		topNpc.state = States.rpg
		topNpc = undefined
	}
	
	objPlayerController.state = States.rpg

	// fade to room
	with(instance_create_layer(0, 0, "Fade", objRoomFade)) {
		// TODO targetRoom should be dynamic
		targetRoom = rpgRoom
		scriptRoomLoaded = getConversation
		npcId = other.id
	}
}