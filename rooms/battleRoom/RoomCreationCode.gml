height = objBattleController.height
width = objBattleController.width
tileSize = objBattleController.tileSize
offsetX = objBattleController.offsetX
offsetY = objBattleController.offsetY
battleGrid = objBattleController.battleGrid

for (var h=0; h<=height; h++) {
	for (var w=0; w<=width; w ++) {
		// outer border
		if (w == 0 || h == 0 || w == width || h == height) {
			with (instance_create_layer(offsetX + w*tileSize+tileSize/2, offsetY+h*tileSize+tileSize/2, "BattleGridLayout", objWall)) {
				ds_grid_set(other.battleGrid, w, h, id)
				gridX = w
				gridY = h
			}
		} else {
			// inner grid
			with (instance_create_layer(offsetX + w*tileSize+tileSize/2, offsetY+h*tileSize+tileSize/2, "BattleGridLayout", objTileBase)) {
				ds_grid_set(other.battleGrid, w, h, id)
				gridX = w
				gridY = h
			}
		}
	}
}

var bossTile = ds_grid_get(battleGrid, width/2, height / 2)
with (objBattleController.boss) {
	curTile = bossTile
	x = curTile.x
	y = curTile.y
	alarm[2] = attackSpeed
	invulnerable = false
	state = objBattleController.startState
}
// left column helper
var helperTile = ds_grid_get(battleGrid, 0, 1)
with (instance_create_layer(helperTile.x, helperTile.y, "Players", objBattleController.leftNpc)) {
	curTile = helperTile
	alarm[0] = moveSpeed
	alarm[1] = attackSpeed 
	state = States.idle
}
// top row helper
var helperTile = ds_grid_get(battleGrid, 1, 0)
with (instance_create_layer(helperTile.x, helperTile.y, "Players", objBattleController.topNpc)) {
	curTile = helperTile	
	alarm[0] = moveSpeed
	alarm[1] = attackSpeed 
	state = States.idle
}

if (objBattleController.postSetupScript != undefined) {
    objBattleController.postSetupScript()
}

objPlayerController.state = States.battle