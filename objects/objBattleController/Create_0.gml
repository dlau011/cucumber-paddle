/// @description objBattleController creation

boss = undefined
startState = undefined // make the boss do this 
dialogueThreshold = 0 // trigger boss.nextDialogue when hp reaches this percentage (default 0), override in battleUtil

leftNpc = undefined
topNpc = undefined

height = 10
width = 10
tileSize = 64
offsetX = 0
offsetY = 0
battleGrid = ds_grid_create(width, height)
postSetupScript = undefined // called after grid setup, right before player.state = battle
