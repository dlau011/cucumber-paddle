/// @description objTestItem create
event_inherited()
description = "something that when used changes the status of another thing"
itemName = "Test Item"

// triggers this conversation with npcBossGhost
triggerConversation = [npcBossGhost, "testItem.pickpup"]


