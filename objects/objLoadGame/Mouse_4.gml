/// @description load game onclick
with (objRPGController) {
	if (file_exists(saveFileName)) {
		ds_map_destroy(saveData)
		saveData = ds_map_secure_load(saveFileName)
		room_restart()
	}
}