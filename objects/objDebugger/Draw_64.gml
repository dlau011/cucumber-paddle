/// debugger
var debugInventory = "inventory: "
var offset = 0
with(objRPGController) {
	var playerInventory = getPlayerInventory()
	for (var i = 0; i < ds_list_size(playerInventory); i++) {
		debugInventory += ds_list_find_value(playerInventory, i) + ","
	}
}
draw_text_color(0,offset,debugInventory, c_white, c_white, c_white, c_white, 1)
//if (instance_exists(npcDog)) {
//	offset += 12
//	draw_text_color(0, offset, string(npcDog.state), c_white, c_white, c_white, c_white, 1)
//}

if (instance_exists(objPlayerController)) {
	offset += 12
	draw_text_color(0, offset, "invul: " + string(objPlayerController.invulnerable), c_white, c_white, c_white, c_white, 1)
	
}
if (instance_exists(objNpcBoss)) {
	offset += 12
	draw_text_color(0, offset, 
		"state: " + string(objNpcBoss.state) + ", xy: " + string(objNpcBoss.x) + "," + string(objNpcBoss.y) +  
		", invul: " + string(objNpcBoss.invulnerable) + 
		", moveSpeed: " + string(objNpcBoss.moveSpeed) + "(speed:" + string(objNpcBoss.speed) + ")" +
		", hp: " + string(objNpcBoss.curHp) + 
		", dmg: " + string(objNpcBoss.damage),
		c_white, c_white, c_white, c_white, 1)	
}
with (objPlayerController) {
	offset += 12
	draw_text_color(0, offset, 
		"state: " + string(state) + ", xy: " + string(x) + "," + string(y) +
		", destXY: " + string(destinationX) + "," + string(destinationY) +
		", invul: " + string(invulnerable) + 
		", speed: " + string(rpgMoveSpeed) + "(" + string(speed) + ")" +
		", hp: " + string(curHp) + "/" + string(maxHp),
		c_white, c_white, c_white, c_white, 1)	
}