/// @description objTextBox init vars and do math

// vars
textBox = sprTextBox
frame = sprPortraitFrame
nameFrame = sprNameBox

portraitWidth = sprite_get_width(sprPlayerPortrait)
portraitHeight = sprite_get_width(sprPlayerPortrait)
textBoxWidth = sprite_get_width(textBox)
//textBoxHeight = sprite_get_height(textBox)
nameFrameHeight = sprite_get_height(nameFrame)
nameFrameWidth = sprite_get_width(nameFrame)

camX = (1280 - (portraitWidth + textBoxWidth)) / 2
camY = 720 - (portraitHeight + nameFrameHeight)
// text box 
choicesDisplayed = false
textBoxX = camX + portraitWidth
textBoxY = camY
nameFrameX = camX
nameFrameY = camY - nameFrameHeight

//text
originalNpc = undefined // needed for multiple conversationalists
texts = undefined // [[id, text], [id, text]...]
choices = undefined // [opt1, opt2, opt3...]
branches = undefined // [label1, label2, label3...]
choice = 0
page = 0 // use to iterate and track index in texts[]
choice = 0 // use to iterate and track index in choices[]
textBufferX = 15
choiceTextBufferX = 35 // makes space for the ">>"
textBufferY = 8
textX = textBoxX + textBufferX
textY = textBoxY + textBufferY
choiceTextX = textBoxX + choiceTextBufferX
textMaxWidth = textBoxWidth - textBufferX
textSep = string_height("H") // height of rows of text
counter = 0
page = 0
speaker = undefined // texts[|page][|0]
// defaults to the 0th frame of the speaker's portrait
portraitIndex = 0
textWrapped = undefined
lenTextWrapped = undefined
pause = false

// nametag
nameX = nameFrameX + nameFrameWidth*.5
nameY = nameFrameY + nameFrameHeight * .5