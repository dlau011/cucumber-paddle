// @description objTextBox next dialogue
// speed up curText 
if (lenTextWrapped != undefined && counter < lenTextWrapped) {
	counter = lenTextWrapped + 1
// next text in texts
} else if (texts != undefined && page < ds_list_size(texts) - 1 && counter >= lenTextWrapped) {
	page++
	speaker = asset_get_index(texts[|page][|0])
    textWrapped = wrapDialogue(texts[|page][|1], textMaxWidth)
    lenTextWrapped = string_length(textWrapped)
	counter = 0
// if choices are undefined then we can end the conversation
// otherwise select choice and find the corresponding branch
// either way we will kill this objTextBox
} else if (choices == undefined || choicesDisplayed) {
	var triggerBattle = false
	speaker = objPlayerController
	// continue conversation if choice branches
	if (branches != undefined && branches[|choice] != "") {
		getConversation(originalNpc, branches[|choice])
	// else handle battle/item/nextDialogue
	} else {
		// next time you talk to this npc you will trigger what next refers to
		if (next != undefined) {
			originalNpc.nextDialogue = next
		}
		// this has to happen right after the npc says something. ie no choice/branch in the dialogue
		if (battle != undefined) {
			triggerBattle = true
		} else if (item != undefined) {
			getItemFromNpc(item)
		}
		originalNpc.state = States.rpg
		objPlayerController.state = States.rpg
	}
	// prevent accidental retrigger of dialogue
	with (objPlayerController) {
		canCreateDialogue = false
		alarm[3] = 10
	}
	instance_destroy()
	// let all the cleanup happen before battle handling
	if (triggerBattle) {
		goToBattle(battle)
	}
} else {
	// wrappedText was fully displayed, next page
	page++	// i dont think this ever gets hit
}

