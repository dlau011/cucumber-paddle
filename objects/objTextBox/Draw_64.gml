if (texts != undefined or choices != undefined) {
// draw textbox (text frame)
draw_sprite(textBox, 0, textBoxX, textBoxY)
// draw frame
draw_sprite(frame, 0, camX, camY)
// draw texts[page]
if (texts != undefined && page < ds_list_size(texts)) {
	// runs this every time we get a new page
	if (textWrapped == undefined) {
		textWrapped = wrapDialogue(texts[|page][|1], textMaxWidth)
		lenTextWrapped = string_length(textWrapped)
		// each npc has a spr portrait with expression at each index, default at 0
		if (ds_list_size(texts[|page]) > 2) {
			portraitIndex = texts[|page][|2]	
		} else {
			portraitIndex = 0
		}
	}
	// draw text
	if (!pause && counter < lenTextWrapped) {
		var curChar = string_char_at(textWrapped, counter)
		counter += 1
		if (counter % 4 == 0 && curChar != ".") {
			audio_play_sound(speaker.voice, 10, false)
		}
		switch (curChar) {
			case ",": pause = true alarm[0] = 10 break
			case ".": pause = true alarm[0] = 15 break
			case "?": pause = true alarm[0] = 20 break
			case "!": pause = true alarm[0] = 25 break
		}
	}
	var substr = string_copy(textWrapped, 1, counter)
	draw_text_ext_color(textX, textY, substr, textSep, textMaxWidth, c_white, c_white, c_white, c_white, 1)
// draw choices for this conversation label
} else if (choices != undefined && page >= ds_list_size(texts)) {
	speaker = objPlayerController
	choicesDisplayed = true
	var i = 0
	var yPadding = 0 // start options on next line
	repeat(ds_list_size(choices)) {
		var textColor = c_white
		if (i == choice) {
			textColor = c_teal
			draw_text_color(choiceTextX - string_width(">> "), textY + yPadding, ">> ", textColor, textColor, textColor, textColor, 1)
		}
		draw_text_ext_color(choiceTextX, textY + yPadding, choices[|i], textSep, textMaxWidth, textColor, textColor, textColor, textColor, 1)
		yPadding += string_height_ext(choices[|i], textSep, textMaxWidth)
		i++
	}
}
draw_sprite(speaker.portrait, portraitIndex, camX, camY)
// draw name
draw_sprite(nameFrame, 0, nameFrameX, nameFrameY)
draw_set_color(c_white)
draw_set_halign(fa_center)draw_set_valign(fa_middle)
draw_text(nameX, nameY, speaker.displayName)
draw_set_halign(fa_left)draw_set_valign(fa_top)
}