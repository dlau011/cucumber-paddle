/// @description choice dialogue handling
if (choicesDisplayed && page >= ds_list_size(texts)) {
	if (keyboard_check_pressed(vk_down) || keyboard_check_pressed(ord("S"))) {
		choice = min(ds_list_size(choices) - 1, choice + 1)
	}
	
	if (keyboard_check_pressed(vk_up) || keyboard_check_pressed(ord("W"))) {
		choice = max(0, choice - 1)
	}
}


