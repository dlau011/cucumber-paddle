/// @description player collision with objTileBase called from player and other is a objTileBase
if (state != States.dialogue) {
	var statuses = ds_map_values_to_array(other.statuses, [])
	for (var i = 0; i < array_length(statuses); i++) {
		statuses[i].collisionScript(id)
	}

	if (invulnerable && alarm[0] <= 0) { // alarm check is so they arent invulnerable forever
		alarm[0] = invulnerableFrames
	}
	if (recentlyHealed && alarm[1] <= 0) {
		alarm[1] = healDelay // DoTs will still apply but wont stack until delay is done
	}
	if (recentlyDmgReduced && alarm[2] <= 0) {
		alarm[2] = dmgReductionDelay	
	}
}