// custom alarm
if (nextStateAlarm >= 0) {
	nextStateAlarm--
	if (nextStateAlarm < 0) {
		state = nextState
		nextState = undefined
	}
}

// scripts in playerMovementUtil
if (state == States.rpg || state == States.battle) {
	playerMovementHandler()
} else if (state == States.knockback) {
	playerKnockbackHandler()
}

// check for objGoToRoom collision
if (!instance_exists(objRoomFade)) {
	var goToRoom = instance_place(x, y, objGoToRoom)
	if (goToRoom != noone) {
		with (instance_create_layer(0,0,"Fade", objRoomFade)) {
			objRPGController.spawnX = goToRoom.spawnX
			objRPGController.spawnY = goToRoom.spawnY
			targetRoom = goToRoom.targetRoom
		}
	}
}

