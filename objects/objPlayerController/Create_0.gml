/// @description init objPlayerController vars
event_inherited()
maxHp = 1000
curHp = maxHp
invulnerable = false
invulnerableFrames = 30 // steps of invul after getting hit
healDelay = 30// steps before player can be healed again
recentlyHealed = false
dmgTaken = 1 // percent damage taken used for damage reduction expressed from 0-1
recentlyDmgReduced = false
dmgReductionDelay = 45

// control player destination
destinationX = undefined
destinationY = undefined

// alarm[0] is invul frames
// alarm[1] is healing invul frames
// alarm[2] is dmg reduction frames
// alarm[3] is canCreateDialogue
// nextStateAlarm is generic
nextState = undefined
nextStateAlarm = -1
// rpg
moveX = 0
moveY = 0
displayName = "PC"
voice = sndDialogue
rpgMoveSpeed = 3
image_speed = 0
canCreateDialogue = true
portrait = sprPlayerPortrait

state = States.rpg
