/// @description Interact with NPC (objPlayerController)

// nothing can happen during dialogue
if (!instance_exists(objTextBox) && !instance_exists(objItemTextBox)) {
	var npcNearby = instance_nearest(x, y, objNpcParent)
	if (npcNearby != noone && distance_to_object(npcNearby) <= 20 && canCreateDialogue) { // canCreateDialogue is a delay for spamming spacebar
		// TODO where should we save npc and player States
		getConversation(npcNearby, npcNearby.nextDialogue)
	}
}
