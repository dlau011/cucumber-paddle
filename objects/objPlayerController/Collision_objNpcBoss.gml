/// @description player collision with boss
if (!invulnerable && state == States.battle) {
	// briefly stop boss
	with (other) {
		if state == States.chase {
			state = States.idle
			// alarm[1] incrementState which defaults to chase if no statelist defined
			// alarm[2] nextAttack
			alarm[1] = min(alarm_get(2), 10)
			speed = 0
		}
	}
	var dmg = round(dmgTaken * other.touchDamage)
	curHp = max(curHp - dmg, 0)
	createDmgText(string(dmg), id, c_white)
	invulnerable = true
	calculateKnockback()
	state = States.knockback
}