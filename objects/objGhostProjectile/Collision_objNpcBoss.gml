/// @description hurt boss and destroy
var dmg = round(other.dmgTaken * damage)
other.curHp = max(0, other.curHp - dmg)
createDmgText(string(dmg), other.id, c_red)
instance_destroy()