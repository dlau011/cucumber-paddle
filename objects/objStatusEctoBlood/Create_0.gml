/// @description init vars
event_inherited()
stunTime = 30 // frames before stun is removed
damage = undefined // combo of the ecto and blood tile that made it
heal = undefined // same here
dmgReduction = .05 // 5% per stack
statusType = TileStatus.ectoBlood
collisionScript = ectoBloodCollision