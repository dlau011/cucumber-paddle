/// movement/attacking
if (state == States.rpg) {

	//Collision Check Horizontal
	if(place_meeting(x+moveX, y, objCollision))
	{
		repeat(abs(moveX))
		{
			if(!place_meeting(x+sign(moveX), y, objCollision)){x += sign(moveX);}
			else {break;}
		}
		moveX = 0;
	}

	//Collision Check Vertical
	if(place_meeting(x, y+moveY, objCollision))
	{
		repeat(abs(moveY))
		{
			if(!place_meeting(x, y+sign(moveY), objCollision)){y += sign(moveY);}
			else {break;}
		}
		moveY = 0;
	}

	//Apply movement
	x += moveX;
	y += moveY;
}