/// @description Wandering
if (state == States.rpg) {
	//stop movement
	moveX = 0;
	moveY = 0;

	//Choose between staying idle of moving
	var idle = choose(0,1);
	//if not staying idle, then choose a random direction to move
	if (idle == 0){
		var dir = choose(1,2,3,4);
		switch(dir){
			case 1: moveX = -rpgMoveSpeed; break;
			case 2: moveX = rpgMoveSpeed; break;
			case 3: moveY = -rpgMoveSpeed; break
			case 4: moveY = rpgMoveSpeed; break;
		}
	}
	//reset every 2-4 seconds
	alarm[11] = random_range(0.5, 2)*room_speed;
}