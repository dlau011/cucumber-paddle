///Initialize objNpcParent 
// all NPCs must have a portrait/name/etc

// dialogue
portrait = undefined
voice = sndDialogue // override for non default typewriter sound
displayName="Name me!"
nextDialogue = "intro"
state = States.rpg
// rpg movement
rpgMoveSpeed = 1;
moveX = 0;
moveY = 0;
alarm[11] = random_range(2,10);
