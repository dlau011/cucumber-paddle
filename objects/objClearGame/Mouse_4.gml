/// @description clear save data onclick
with (objRPGController) {
	saveData = ds_map_create()
	ds_map_secure_save(saveData, saveFileName)
	room_restart()
}	