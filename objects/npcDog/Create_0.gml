/// @description init vars
event_inherited()
moveSpeed = 2 // movementScript once per second?
attackScript = dogAttack
attackSpeed = 360

// dialogue
portrait = sprDogPortrait
voice = sndDialogue
displayName="Dog"
nextDialogue = "intro"