/// @description init objNpcPartyMember vars
// all objNpcPartyMember are able to be part of battle otherwise use objNpcParent
event_inherited()
// all possible stats used to make stats
damage = undefined
slow = undefined
slowTime = undefined
heal = undefined

// attack
attackScripts = ds_list_create()
attackScript = undefined
attackSpeed = undefined
readyAttack = false // when this is true, attack when in correct spot
alarm[1] = 0 // set this to attackSpeed attackScript should be defined to call scripts in attackScripts 

// move
movementScript = npcMovement
moveSpeed = undefined
alarm[0] = 0 // set this to moveSpeed after it is defined

