// draw textbox (text frame)
draw_sprite(textBox, 0, textBoxX, textBoxY)
// draw frame
draw_sprite(frame, 0, camX, camY)
// draw text
if (textWrapped == undefined) {
	textWrapped = wrapDialogue(item.description, textMaxWidth)
	lenTextWrapped = string_length(textWrapped)
}
draw_text_ext_color(textX, textY, textWrapped, textSep, textMaxWidth, c_white, c_white, c_white, c_white, 1)

// draw portrait
draw_sprite(item.sprite_index, 0, camX, camY)
// draw name
draw_sprite(nameFrame, 0, nameFrameX, nameFrameY)
draw_set_color(c_white)
draw_set_halign(fa_center)draw_set_valign(fa_middle)
draw_text(nameX, nameY, item.itemName)
draw_set_halign(fa_left)draw_set_valign(fa_top)
