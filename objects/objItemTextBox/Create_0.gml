/// @description objItemInfo create
// derived from objTextBox code removed speakers, choices, etc
// vars
textBox = sprTextBox
frame = sprPortraitFrame
nameFrame = sprNameBox

portraitWidth = sprite_get_width(sprPlayerPortrait)
portraitHeight = sprite_get_width(sprPlayerPortrait)
textBoxWidth = sprite_get_width(textBox)
//textBoxHeight = sprite_get_height(textBox)
nameFrameHeight = sprite_get_height(nameFrame)
nameFrameWidth = sprite_get_width(nameFrame)

camX = (1280 - (portraitWidth + textBoxWidth)) / 2
camY = 720 - (portraitHeight + nameFrameHeight)
// text box 
choicesDisplayed = false
textBoxX = camX + portraitWidth
textBoxY = camY
nameFrameX = camX
nameFrameY = camY - nameFrameHeight

//text
item = undefined // instance of objItemParent
textBufferX = 15
textBufferY = 8
textX = textBoxX + textBufferX
textY = textBoxY + textBufferY
textMaxWidth = textBoxWidth - textBufferX
textSep = string_height("H") // height of rows of text
counter = 0
textWrapped = undefined
lenTextWrapped = undefined
pause = false

// nametag
nameX = nameFrameX + nameFrameWidth*.5
nameY = nameFrameY + nameFrameHeight * .5