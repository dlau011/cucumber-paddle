// @description objItemTextBox destroy textbox; maybe trigger conversation

with (item) {
	// if the item has no data in the saveData map, this is the first time we've picked it up
	if (itemSaveData == undefined || itemSaveData != true) {
		if (triggerConversation != undefined) {
			getConversation(triggerConversation[0], triggerConversation[1])	
		}
		// set to true meaning picked up
		ds_map_replace(objRPGController.saveData, key, true)
	}
	// add name to player inventory list
	addPlayerInventory(id)
}


instance_destroy()
instance_destroy(item)

// allow player to move again
objPlayerController.state = States.rpg

