/// @description init ghost boss vars and scripts
event_inherited()
portrait = sprBossGhostPortrait
defaultSprite = sprBossGhost
// movement
// boss movement is handled by the Step event
defaultMoveSpeed = 1 // do not change
moveSpeed = defaultMoveSpeed
//States 
// attack
attackSpeed = 200 // time between attacks // TODO write script to vary attack patterns
damage = 20
// STAGE 1 ATTACKS
// charge 
curPath = undefined
chargeAttackStates = ds_list_create()
ds_list_add(chargeAttackStates, States.moveToDestination, States.rotateInPlace, States.chargeEdge)
// eruption attack
eruptionAttackStates = ds_list_create()
eruptionDamage = 50
ds_list_add(eruptionAttackStates, States.prepareEruption, States.erupt)
// furniture knockback
furnitureKnockbackStates = ds_list_create()
ds_list_add(furnitureKnockbackStates, States.stopAndGlow, States.moveToDestination, States.endFurnitureKnockback)
// hp
maxHp = 6000
curHp = maxHp
// alarms
// alarm[0] is invul frames
// alarm[1] increments state only set it when it's <= 0, meaning the statelist is done
// alarm[2] = attackSpeed

// dialogue
portraitIndex = 0
voice = sndDialogue
displayName="Ghost"
nextDialogue = "intro"
