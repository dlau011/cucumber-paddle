/// @description bossGhost step move and attack
event_inherited()
if (state != States.dialogue && state != States.rpg) {
	switch (state) {
		// attacks are defined by States (e.g. moving to a tile, rotating in place, following a path)
		case States.chase:
			if (!place_meeting(x, y, objPlayerController)) {
				move_towards_point(objPlayerController.x, objPlayerController.y, defaultMoveSpeed)
			}
			break
		case States.moveToDestination:
			moveToDestination()
			break
		case States.stopAndGlow:
			stopAndGlow()
			break
		case States.endFurnitureKnockback:
			endFurnitureKnockback()
			break
		case States.rotateInPlace:
			rotateInPlace()
			break
		case States.chargeEdge:
			chargeEdge()
			break
		case States.prepareEruption:
			prepareEruption()
			break
		case States.erupt:
			erupt()
			break
		case States.idle:
			break
		default:
			show_debug_message("missing state: " + string(state))
			break
	}
}