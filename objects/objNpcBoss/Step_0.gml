/// objNpcBoss step event
event_inherited()
// this looks to trigger the next dialogue event or leave battleRoom
if (state != States.rpg && state != States.dialogue && curHp <= (maxHp*dialogueThreshold)) {
	// remove all statuses
	with (objStatusHandlerParent) {
		instance_destroy()
	}
	with (objStatusParent) {
		instance_destroy()	
	}
	with (objStatusTileParent) {
		instance_destroy()
	}
	// freeze helpers
	with (objBattleController.topNpc) {
		alarm[0] = -1
		alarm[1] = -1
		speed = 0
		state = States.idle
	}
	with (objBattleController.leftNpc) {
		alarm[0] = -1
		alarm[1] = -1
		speed = 0
		state = States.idle
	}
	// freeze boss
	path_end()
	speed = 0
	state = States.rpg
	invulnerable = true
	image_angle = 0
	alarm[1] = -1
	alarm[2] = -1
	destinationX = undefined
	destinationY = undefined
	currentStateList = undefined
	stateIndex = 0
	if (curHp <= 0) {
		resolveBattle(id)
	} else {
		getConversation(id, nextDialogue)
	}
	
}