/// @description boss collision with objTileBase called from objBossParent and other is a objTileBase
if (state != States.dialogue) {
	var statuses = ds_map_values_to_array(other.statuses, [])
	for (var i = 0; i < array_length(statuses); i++) {
		statuses[i].collisionScript(id)
	}

	if (invulnerable && alarm[0] <= 0) {
		alarm[0] = invulnerableFrames 
	}

	if (!recentlyHealed) {
		// TODO
			//alarm[1] = healDelay
	}
}