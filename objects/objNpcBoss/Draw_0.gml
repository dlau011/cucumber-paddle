/// @description draw health
draw_self()
if (state != States.dialogue and state != States.rpg) {
	var width = sprite_get_width(sprite_index)
	var height = sprite_get_height(sprite_index)
	draw_healthbar (x-width*.5, (y-height*.5) - 15, x+width*.5, (y-height*.5) - 10, curHp/maxHp*100, c_black, c_red, c_green, 0, true, true)
}