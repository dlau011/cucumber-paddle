/// @description init objNpcBoss vars
event_inherited()
// alarm 0=invul, 1=incrementState, 2=nextAttack
// all objNpcBoss are able to act as boss in fights
maxHp = undefined
curHp = undefined
moveSpeed = undefined
defaultMoveSpeed = undefined
touchDamage = 30 // damage to player on touch
defaultSprite = undefined // used to reset sprite between battles

invulnerable = false
invulnerableFrames = 30 // steps of invul after getting hit
healDelay = 30
recentlyHealed = false // steps before player can be healed again
dmgTaken = 1 // percent damage taken used for damage reduction expressed from 0-1
canBeSlowed = true // slows cannot stack
canBeStunned = true // stuns do not stack
dialogueThreshold = undefined // trigger dialogue when curHp = maxHp * dialogueThreshold 

damage = undefined
slow = undefined
slowTime = undefined
heal = undefined

//States
stage = 0 // stage of boss during battle
stateIndex = 0 // use to iterate through States lists and reset to 0 when done
// represents one attack/move/action/etc using multiple States
currentStateList = undefined
nextAttack = undefined // is a script e.g. bossGhostErupt
destinationX = undefined // used with States.moveToDestination
destinationY = undefined 