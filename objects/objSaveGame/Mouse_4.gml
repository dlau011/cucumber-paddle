/// @description save game on click
with (objRPGController) {
	ds_map_replace(saveData, "playerState", objPlayerController.state)
	ds_map_secure_save(saveData, saveFileName)	
}