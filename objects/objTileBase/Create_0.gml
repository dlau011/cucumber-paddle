/// @description objTileBase init tile grid position
// where the tile is in the ds_grid as opposed to its literal pixel x/y position
gridX = undefined
gridY = undefined
// all objStatusParent children that can be applied to the base tile
statuses = ds_map_create()
enum TileStatus {
	blood,
	ectoplasm,
	ectoBlood,
	bossAttack,
	numTileStatuses
}
