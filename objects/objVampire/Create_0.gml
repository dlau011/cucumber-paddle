/// init vars
event_inherited()
// attack
attackScript = vampireAttack
attackScripts[|0] = vampireAttackBloodDrain
attackScripts[|1] = vampireAttackBloodSuck
attackSpeed = 100
alarm[1] = attackSpeed
damage = 40
// move
moveSpeed = 1

