/// init vars
event_inherited()
// attack
attackScript = ghostAttack
attackScripts[| 0] = ghostAttackEctoplasm
attackScripts[| 1] = ghostAttackProjectile
attackSpeed = 240
// move
moveSpeed = 2

// stats
slow = .8
slowTime = 300
heal = 30 // ectoplasm heal
damage = 5