/// @description init vars room fade
a = 0 // alpha
fade = 1 // 1 for fade in or out
targetRoom = undefined

// needed for things like triggering dialogue as soon as the room loads
scriptRoomLoaded = undefined
npcId = undefined // to know which npc dialogue to trigger