/// @description fade
a = clamp(a + (fade * 0.05), 0, 1) // increasing alpha by .05 every frame

// start fading to black, persist object to next room 
if (a == 1 && fade == 1) {
	fade = -1 // fading in we'll decrease alpha by .05 every frame
	room_goto(targetRoom)
} else if (a == 0 && fade == -1) { // done fading in
	instance_destroy()
	if (scriptRoomLoaded != undefined) {
		scriptRoomLoaded(npcId, npcId.nextDialogue)
		npcId = undefined // clear it for next use
		scriptRoomLoaded = undefined
	}
}

draw_set_color(c_black)
draw_set_alpha(a)
draw_rectangle(0, 0, room_width, room_height, false)
draw_set_alpha(1)