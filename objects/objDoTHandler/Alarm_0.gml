/// @description deal dot and subtract hits
if (numOfHits > 0) {
	var netDmg = round(damage*(target.dmgTaken))
	numOfHits -= 1
	target.curHp -= netDmg
	var color
	if (target.object_index == objPlayerController.object_index) {
		color = c_white
	} else {
		color = c_red	
	}
	createDmgText(string(netDmg), target, color)
	alarm[0] = framesBetweenHits
} else {
	instance_destroy()
}
