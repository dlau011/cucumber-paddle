/// @description Create vars for RPG player
global.dialogueDog = importJson("dialogueDog.txt")
global.dialogueGhostBoss = importJson("dialogueGhostBoss.txt")
global.dialogueCultist = importJson("dialogueCultist.txt")

saveData = ds_map_create()
saveFileName = "SaveData.sav"

spawnX = 222
spawnY = 158

randomise() 