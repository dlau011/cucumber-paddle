/// @description draw itself
if (target != undefined && randX == undefined) {
	randX = target.x+irandom_range(-30,30)
	randY = target.y - target.sprite_height + irandom_range(-5,0)
}
if (opacity > 0) {
	draw_text_colour(randX, randY, text, color, color, color, color, opacity/255)
	opacity -= rate
} else {
	instance_destroy()	
}
