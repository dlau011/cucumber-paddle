{
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 7,
  "bbox_right": 20,
  "bbox_top": 14,
  "bbox_bottom": 36,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 28,
  "height": 37,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"0a956910-ec00-48b6-912e-f41f6538bae8","path":"sprites/sprRPGPlayerDown/sprRPGPlayerDown.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0a956910-ec00-48b6-912e-f41f6538bae8","path":"sprites/sprRPGPlayerDown/sprRPGPlayerDown.yy",},"LayerId":{"name":"f8a25267-4725-42b9-9eda-3e9739292e6b","path":"sprites/sprRPGPlayerDown/sprRPGPlayerDown.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprRPGPlayerDown","path":"sprites/sprRPGPlayerDown/sprRPGPlayerDown.yy",},"resourceVersion":"1.0","name":"0a956910-ec00-48b6-912e-f41f6538bae8","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f9907ffe-1575-40ec-a2ca-9199a27846a9","path":"sprites/sprRPGPlayerDown/sprRPGPlayerDown.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f9907ffe-1575-40ec-a2ca-9199a27846a9","path":"sprites/sprRPGPlayerDown/sprRPGPlayerDown.yy",},"LayerId":{"name":"f8a25267-4725-42b9-9eda-3e9739292e6b","path":"sprites/sprRPGPlayerDown/sprRPGPlayerDown.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprRPGPlayerDown","path":"sprites/sprRPGPlayerDown/sprRPGPlayerDown.yy",},"resourceVersion":"1.0","name":"f9907ffe-1575-40ec-a2ca-9199a27846a9","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"49366887-fcc5-4758-b565-2209a2e7193d","path":"sprites/sprRPGPlayerDown/sprRPGPlayerDown.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"49366887-fcc5-4758-b565-2209a2e7193d","path":"sprites/sprRPGPlayerDown/sprRPGPlayerDown.yy",},"LayerId":{"name":"f8a25267-4725-42b9-9eda-3e9739292e6b","path":"sprites/sprRPGPlayerDown/sprRPGPlayerDown.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprRPGPlayerDown","path":"sprites/sprRPGPlayerDown/sprRPGPlayerDown.yy",},"resourceVersion":"1.0","name":"49366887-fcc5-4758-b565-2209a2e7193d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"90cb7105-52a1-4521-bd0a-eff482a7bc27","path":"sprites/sprRPGPlayerDown/sprRPGPlayerDown.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"90cb7105-52a1-4521-bd0a-eff482a7bc27","path":"sprites/sprRPGPlayerDown/sprRPGPlayerDown.yy",},"LayerId":{"name":"f8a25267-4725-42b9-9eda-3e9739292e6b","path":"sprites/sprRPGPlayerDown/sprRPGPlayerDown.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprRPGPlayerDown","path":"sprites/sprRPGPlayerDown/sprRPGPlayerDown.yy",},"resourceVersion":"1.0","name":"90cb7105-52a1-4521-bd0a-eff482a7bc27","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sprRPGPlayerDown","path":"sprites/sprRPGPlayerDown/sprRPGPlayerDown.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 4.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"eff4c427-72c4-419c-8295-dc67bceeda10","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0a956910-ec00-48b6-912e-f41f6538bae8","path":"sprites/sprRPGPlayerDown/sprRPGPlayerDown.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"654f3340-7740-4d44-be4d-8170a58fe7f5","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f9907ffe-1575-40ec-a2ca-9199a27846a9","path":"sprites/sprRPGPlayerDown/sprRPGPlayerDown.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"24205a54-dc6b-4b24-b952-738feb838a59","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"49366887-fcc5-4758-b565-2209a2e7193d","path":"sprites/sprRPGPlayerDown/sprRPGPlayerDown.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7e33ef12-4e01-4d25-b251-6edb8904f510","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"90cb7105-52a1-4521-bd0a-eff482a7bc27","path":"sprites/sprRPGPlayerDown/sprRPGPlayerDown.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sprRPGPlayerDown","path":"sprites/sprRPGPlayerDown/sprRPGPlayerDown.yy",},
    "resourceVersion": "1.3",
    "name": "sprRPGPlayerDown",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"f8a25267-4725-42b9-9eda-3e9739292e6b","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Player",
    "path": "folders/Assets/Sprites/Player.yy",
  },
  "resourceVersion": "1.0",
  "name": "sprRPGPlayerDown",
  "tags": [],
  "resourceType": "GMSprite",
}