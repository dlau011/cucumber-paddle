{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 33,
  "bbox_right": 1236,
  "bbox_top": 5,
  "bbox_bottom": 687,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 1280,
  "height": 720,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"30c1cb0b-5d51-4220-964e-6a07b1fc94dd","path":"sprites/sprFrontHall/sprFrontHall.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"30c1cb0b-5d51-4220-964e-6a07b1fc94dd","path":"sprites/sprFrontHall/sprFrontHall.yy",},"LayerId":{"name":"3c6901bd-8119-4bc5-a8ae-98420c754ed9","path":"sprites/sprFrontHall/sprFrontHall.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprFrontHall","path":"sprites/sprFrontHall/sprFrontHall.yy",},"resourceVersion":"1.0","name":"30c1cb0b-5d51-4220-964e-6a07b1fc94dd","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sprFrontHall","path":"sprites/sprFrontHall/sprFrontHall.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"b2124e58-9f1d-4f25-8bae-dd60454b02f7","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"30c1cb0b-5d51-4220-964e-6a07b1fc94dd","path":"sprites/sprFrontHall/sprFrontHall.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sprFrontHall","path":"sprites/sprFrontHall/sprFrontHall.yy",},
    "resourceVersion": "1.3",
    "name": "sprFrontHall",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"3c6901bd-8119-4bc5-a8ae-98420c754ed9","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Sprites",
    "path": "folders/Assets/Sprites.yy",
  },
  "resourceVersion": "1.0",
  "name": "sprFrontHall",
  "tags": [],
  "resourceType": "GMSprite",
}