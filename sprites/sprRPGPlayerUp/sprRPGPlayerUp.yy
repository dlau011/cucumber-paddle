{
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 7,
  "bbox_right": 20,
  "bbox_top": 0,
  "bbox_bottom": 36,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 28,
  "height": 37,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"e8720cbb-0536-484e-ab8a-a109cdf68541","path":"sprites/sprRPGPlayerUp/sprRPGPlayerUp.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e8720cbb-0536-484e-ab8a-a109cdf68541","path":"sprites/sprRPGPlayerUp/sprRPGPlayerUp.yy",},"LayerId":{"name":"72d200e5-bc93-436c-b5c4-836efbfc625c","path":"sprites/sprRPGPlayerUp/sprRPGPlayerUp.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprRPGPlayerUp","path":"sprites/sprRPGPlayerUp/sprRPGPlayerUp.yy",},"resourceVersion":"1.0","name":"e8720cbb-0536-484e-ab8a-a109cdf68541","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f03535af-f24f-4515-9f97-7105bdd9648e","path":"sprites/sprRPGPlayerUp/sprRPGPlayerUp.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f03535af-f24f-4515-9f97-7105bdd9648e","path":"sprites/sprRPGPlayerUp/sprRPGPlayerUp.yy",},"LayerId":{"name":"72d200e5-bc93-436c-b5c4-836efbfc625c","path":"sprites/sprRPGPlayerUp/sprRPGPlayerUp.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprRPGPlayerUp","path":"sprites/sprRPGPlayerUp/sprRPGPlayerUp.yy",},"resourceVersion":"1.0","name":"f03535af-f24f-4515-9f97-7105bdd9648e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"94b41da0-c6a4-40a1-a81f-594be7b00570","path":"sprites/sprRPGPlayerUp/sprRPGPlayerUp.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"94b41da0-c6a4-40a1-a81f-594be7b00570","path":"sprites/sprRPGPlayerUp/sprRPGPlayerUp.yy",},"LayerId":{"name":"72d200e5-bc93-436c-b5c4-836efbfc625c","path":"sprites/sprRPGPlayerUp/sprRPGPlayerUp.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprRPGPlayerUp","path":"sprites/sprRPGPlayerUp/sprRPGPlayerUp.yy",},"resourceVersion":"1.0","name":"94b41da0-c6a4-40a1-a81f-594be7b00570","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"8df9aa5a-2a96-4940-8201-0fb0fb81c22d","path":"sprites/sprRPGPlayerUp/sprRPGPlayerUp.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8df9aa5a-2a96-4940-8201-0fb0fb81c22d","path":"sprites/sprRPGPlayerUp/sprRPGPlayerUp.yy",},"LayerId":{"name":"72d200e5-bc93-436c-b5c4-836efbfc625c","path":"sprites/sprRPGPlayerUp/sprRPGPlayerUp.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprRPGPlayerUp","path":"sprites/sprRPGPlayerUp/sprRPGPlayerUp.yy",},"resourceVersion":"1.0","name":"8df9aa5a-2a96-4940-8201-0fb0fb81c22d","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sprRPGPlayerUp","path":"sprites/sprRPGPlayerUp/sprRPGPlayerUp.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 4.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"557b70ba-59e5-48ec-8858-e92d1fbd3d2f","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e8720cbb-0536-484e-ab8a-a109cdf68541","path":"sprites/sprRPGPlayerUp/sprRPGPlayerUp.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8a3db66b-fbb1-492b-ae7d-0b2d592a19af","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f03535af-f24f-4515-9f97-7105bdd9648e","path":"sprites/sprRPGPlayerUp/sprRPGPlayerUp.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2ef5af0d-31b2-4888-a0bd-d3f01283e1b1","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"94b41da0-c6a4-40a1-a81f-594be7b00570","path":"sprites/sprRPGPlayerUp/sprRPGPlayerUp.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"66f99331-0f4c-47c2-baae-af9c7e3f4b7f","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8df9aa5a-2a96-4940-8201-0fb0fb81c22d","path":"sprites/sprRPGPlayerUp/sprRPGPlayerUp.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sprRPGPlayerUp","path":"sprites/sprRPGPlayerUp/sprRPGPlayerUp.yy",},
    "resourceVersion": "1.3",
    "name": "sprRPGPlayerUp",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"72d200e5-bc93-436c-b5c4-836efbfc625c","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Player",
    "path": "folders/Assets/Sprites/Player.yy",
  },
  "resourceVersion": "1.0",
  "name": "sprRPGPlayerUp",
  "tags": [],
  "resourceType": "GMSprite",
}