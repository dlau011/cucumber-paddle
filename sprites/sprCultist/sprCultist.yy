{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 3,
  "bbox_right": 28,
  "bbox_top": 0,
  "bbox_bottom": 31,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 32,
  "height": 32,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"248cf42b-fa4f-475e-b7b7-f70cc7fe219e","path":"sprites/sprCultist/sprCultist.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"248cf42b-fa4f-475e-b7b7-f70cc7fe219e","path":"sprites/sprCultist/sprCultist.yy",},"LayerId":{"name":"49431511-9787-4890-870f-b7922d3c5a26","path":"sprites/sprCultist/sprCultist.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprCultist","path":"sprites/sprCultist/sprCultist.yy",},"resourceVersion":"1.0","name":"248cf42b-fa4f-475e-b7b7-f70cc7fe219e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"fb4a2c99-a450-413e-9cda-fd43ec148675","path":"sprites/sprCultist/sprCultist.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"fb4a2c99-a450-413e-9cda-fd43ec148675","path":"sprites/sprCultist/sprCultist.yy",},"LayerId":{"name":"49431511-9787-4890-870f-b7922d3c5a26","path":"sprites/sprCultist/sprCultist.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprCultist","path":"sprites/sprCultist/sprCultist.yy",},"resourceVersion":"1.0","name":"fb4a2c99-a450-413e-9cda-fd43ec148675","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1da5fbbe-0534-4b0d-b228-8c5dab3b1baf","path":"sprites/sprCultist/sprCultist.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1da5fbbe-0534-4b0d-b228-8c5dab3b1baf","path":"sprites/sprCultist/sprCultist.yy",},"LayerId":{"name":"49431511-9787-4890-870f-b7922d3c5a26","path":"sprites/sprCultist/sprCultist.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprCultist","path":"sprites/sprCultist/sprCultist.yy",},"resourceVersion":"1.0","name":"1da5fbbe-0534-4b0d-b228-8c5dab3b1baf","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"69d1c32d-1333-4a14-9014-70e300de6070","path":"sprites/sprCultist/sprCultist.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"69d1c32d-1333-4a14-9014-70e300de6070","path":"sprites/sprCultist/sprCultist.yy",},"LayerId":{"name":"49431511-9787-4890-870f-b7922d3c5a26","path":"sprites/sprCultist/sprCultist.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprCultist","path":"sprites/sprCultist/sprCultist.yy",},"resourceVersion":"1.0","name":"69d1c32d-1333-4a14-9014-70e300de6070","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"9f4d4470-e250-4a7e-95d4-dfbee8956658","path":"sprites/sprCultist/sprCultist.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9f4d4470-e250-4a7e-95d4-dfbee8956658","path":"sprites/sprCultist/sprCultist.yy",},"LayerId":{"name":"49431511-9787-4890-870f-b7922d3c5a26","path":"sprites/sprCultist/sprCultist.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprCultist","path":"sprites/sprCultist/sprCultist.yy",},"resourceVersion":"1.0","name":"9f4d4470-e250-4a7e-95d4-dfbee8956658","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"409818fe-7f30-4c22-ab32-549343b6e18e","path":"sprites/sprCultist/sprCultist.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"409818fe-7f30-4c22-ab32-549343b6e18e","path":"sprites/sprCultist/sprCultist.yy",},"LayerId":{"name":"49431511-9787-4890-870f-b7922d3c5a26","path":"sprites/sprCultist/sprCultist.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprCultist","path":"sprites/sprCultist/sprCultist.yy",},"resourceVersion":"1.0","name":"409818fe-7f30-4c22-ab32-549343b6e18e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0da9b1c6-ccd5-401f-b31a-7181dc1a6df7","path":"sprites/sprCultist/sprCultist.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0da9b1c6-ccd5-401f-b31a-7181dc1a6df7","path":"sprites/sprCultist/sprCultist.yy",},"LayerId":{"name":"49431511-9787-4890-870f-b7922d3c5a26","path":"sprites/sprCultist/sprCultist.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprCultist","path":"sprites/sprCultist/sprCultist.yy",},"resourceVersion":"1.0","name":"0da9b1c6-ccd5-401f-b31a-7181dc1a6df7","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"35e9ed78-fe75-4e99-a728-2037afc9d6bf","path":"sprites/sprCultist/sprCultist.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"35e9ed78-fe75-4e99-a728-2037afc9d6bf","path":"sprites/sprCultist/sprCultist.yy",},"LayerId":{"name":"49431511-9787-4890-870f-b7922d3c5a26","path":"sprites/sprCultist/sprCultist.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sprCultist","path":"sprites/sprCultist/sprCultist.yy",},"resourceVersion":"1.0","name":"35e9ed78-fe75-4e99-a728-2037afc9d6bf","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sprCultist","path":"sprites/sprCultist/sprCultist.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 8.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"ecd4b8d8-e2e0-4a60-94ce-534bdda61901","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"248cf42b-fa4f-475e-b7b7-f70cc7fe219e","path":"sprites/sprCultist/sprCultist.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b1d59828-6d1b-44ca-b051-ff8db408d7b1","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fb4a2c99-a450-413e-9cda-fd43ec148675","path":"sprites/sprCultist/sprCultist.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0a6da2a1-d18f-4089-844b-d3a528878d9b","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1da5fbbe-0534-4b0d-b228-8c5dab3b1baf","path":"sprites/sprCultist/sprCultist.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4179f421-0a81-4991-b9cb-b606f2b6ac7f","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"69d1c32d-1333-4a14-9014-70e300de6070","path":"sprites/sprCultist/sprCultist.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0f88e34a-42a1-4f22-9d63-68c1cd61703a","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9f4d4470-e250-4a7e-95d4-dfbee8956658","path":"sprites/sprCultist/sprCultist.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"21708f8e-d0fd-4760-a013-632052a8f671","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"409818fe-7f30-4c22-ab32-549343b6e18e","path":"sprites/sprCultist/sprCultist.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d572a41d-1f93-4c50-be58-43725f59f074","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0da9b1c6-ccd5-401f-b31a-7181dc1a6df7","path":"sprites/sprCultist/sprCultist.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"35fc4714-fe34-4382-b13a-dcfc1b8d468d","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"35e9ed78-fe75-4e99-a728-2037afc9d6bf","path":"sprites/sprCultist/sprCultist.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 16,
    "yorigin": 16,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sprCultist","path":"sprites/sprCultist/sprCultist.yy",},
    "resourceVersion": "1.3",
    "name": "sprCultist",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"49431511-9787-4890-870f-b7922d3c5a26","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "cultist",
    "path": "folders/RPG/Objects/NPC/RPG/cultist.yy",
  },
  "resourceVersion": "1.0",
  "name": "sprCultist",
  "tags": [],
  "resourceType": "GMSprite",
}